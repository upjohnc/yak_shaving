#! /usr/bin/env python3

import subprocess
from pathlib import Path

yak_uri = 'git@bitbucket.org:upjohnc/yak_shaving.git'
yak_url = 'https://bitbucket.org/upjohnc/yak_shaving'


def install_pre_commit():
    pre_commit_command = 'pre-commit install'
    _ = subprocess.Popen(pre_commit_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)  # noqa


def root_directory():
    root_dir_command = 'git rev-parse --show-toplevel'
    root_dir_stdout = subprocess.Popen(root_dir_command.split(), stdout=subprocess.PIPE)
    root_dir = root_dir_stdout.stdout.read().decode().splitlines()[0]
    return root_dir


def master_hash():
    git_call = subprocess.Popen(f'git ls-remote {yak_uri}'.split(), stdout=subprocess.PIPE)
    branches_bytes = git_call.stdout.read()
    branches_string = branches_bytes.decode()
    master_info = [i.split('\t') for i in branches_string.splitlines() if 'master' in i][0]
    return master_info[0]


def retrieve_file(hash_value, file_name):
    url_root = f'{yak_url}/raw/{hash_value}/{file_name}'
    file_bytes = subprocess.Popen(f'curl -s {url_root}'.split(), stdout=subprocess.PIPE)
    file_content = file_bytes.stdout.read().decode()
    return file_content


def save_file_contents(directory, file_name, file_content):
    with open(Path(directory) / file_name, 'w') as f:
        f.write(file_content)


def copy_files():
    base_dir = root_directory()
    hash_ = master_hash()

    def do_file(file_name):
        file_contents = retrieve_file(hash_, file_name)
        save_file_contents(base_dir, file_name, file_contents)

    files_ = ('.pre-commit-config.yaml', '.isort.cfg', 'makefile', '.gitignore')
    list(map(do_file, files_))


if __name__ == '__main__':
    print('copying files')
    copy_files()
    print('pre install')
    install_pre_commit()
