# Yak Shaving

Simple script to set up the regular files in a new project: `makefile`, `.isort.cfg`, `.pre-commit-config.yaml`

## Usage

- Add `yak_shaving.py` to `~`
- `chmod a+x ~/yak_shaving.py`
- Add alias `alias yak_shaving=~/yak_shaving.py`

## Note

- needs to be a git repo - `git init`
- `pre-commit` needs to be installed globally
    - `brew install pre-commit`
- `gnu make` needs to be >= 3.8.2

## Steps: Project Setup

- poetry
  - run poetry init
  - add black to pyproject.toml
  - poetry install
- add .pre-commit.yaml
  - precommit init
- add .isort.cfg
- add .pytest.ini
- add makefile
  - `make run` for dropping into back in running a container
  - `make test` running tests when in a container
- `docker-compose.yaml` with volumes for src->app and tests->test
