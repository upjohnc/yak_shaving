# note need make >= 3.8.2
SHELL := bash
.ONESHELL:
.RECIPEPREFIX = >

root_dir = ./src

test:
> PYTHONPATH=$(root_dir) pytest -v

test-watch:
> find . -type f -name "*.py" | entr env PYTHONPATH=$(root_dir) pytest -v

isort:
> isort -rc . .isort.cfg

mypy:
> mypy $(root_dir)
